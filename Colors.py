import cv2
import numpy as np




#############   Video Settings   #############################
cap = cv2.VideoCapture(0,cv2.CAP_DSHOW)
cap.set(3,720)
cap.set(4,1280)
cap.set(10,120)
################################################################)
def draw(mask,color):
    cont,_=cv2.findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
        
    for c in cont:
            epsilon = 0.020*cv2.arcLength(c,True)
            approx = cv2.approxPolyDP(c,epsilon,True)
            x,y,w,h = cv2.boundingRect(approx)
            area = cv2.contourArea(c)
###### Comentar el if que no deseas mostrar y
###### dejar solamente la figura que deseas mostrar #####         
            #Todas las figuras 
            if area > 3000:
              target=cv2.moments(c)
              if (target["m00"]==00): target["m00"]=1
              x=int(target["m10"]/target["m00"])
              y=int(target["m01"]/target["m00"])
            ###  Centro del objeto
              cv2.circle(frame,(x,y),5,(0,0,0),-1)
              newContour=cv2.convexHull(c)
              cv2.drawContours(frame,[newContour],0,(color),3)
            ##Triangulo
            #if len(approx)==3:
            #    target=cv2.moments(c)
            #    if (target["m00"]==00): target["m00"]=1
            #    x=int(target["m10"]/target["m00"])
            #    y=int(target["m01"]/target["m00"])
            #    cv2.circle(frame,(x,y),5,(0,0,0),-1)
            #    newContour=cv2.convexHull(c)
            #    cv2.drawContours(frame,[newContour],0,color,3)
            ##4 lados
            #if len(approx)==4:
            #    target=cv2.moments(c)
            #    if (target["m00"]==00): target["m00"]=1
            #    x=int(target["m10"]/target["m00"])
            #    y=int(target["m01"]/target["m00"])
            #    cv2.circle(frame,(x,y),5,(0,0,0),-1)
            #    newContour=cv2.convexHull(c)
            #    cv2.drawContours(frame,[newContour],0,color,3)
            ## Pentagono
            #if len(approx)==5:
            #    target=cv2.moments(c)
            #    if (target["m00"]==00): target["m00"]=1
            #    x=int(target["m10"]/target["m00"])
            #    y=int(target["m01"]/target["m00"])
            #    cv2.circle(frame,(x,y),5,(0,0,0),-1)
            #    newContour=cv2.convexHull(c)
            #    cv2.drawContours(frame,[newContour],0,color,3)
            ###Hexagono
            #if len(approx)==6:
            #    target=cv2.moments(c)
            #    if (target["m00"]==00): target["m00"]=1
            #    x=int(target["m10"]/target["m00"])
            #    y=int(target["m01"]/target["m00"])
            #    cv2.circle(frame,(x,y),5,(0,0,0),-1)
            #    newContour=cv2.convexHull(c)
            #    cv2.drawContours(frame,[newContour],0,color,3)
            ###Circulo
            #if len(approx)>7:
            #    target=cv2.moments(c)
            #    if (target["m00"]==00): target["m00"]=1
            #    x=int(target["m10"]/target["m00"])
            #    y=int(target["m01"]/target["m00"])
            #    cv2.circle(frame,(x,y),5,(0,0,0),-1)
            #    newContour=cv2.convexHull(c)
            #    cv2.drawContours(frame,[newContour],0,color,3)
            ###  Buscar cpunto central o coordenadas del centro del objeto
            #if (target["m00"]==00): target["m00"]=1
            #x=int(target["m10"]/target["m00"])
            #y=int(target["m01"]/target["m00"])
            ####  Centro del objeto
            #cv2.circle(frame,(x,y),5,(0,0,0),-1)
            #newContour=cv2.convexHull(c)
            #cv2.drawContours(frame,[newContour],0,color,3)

 #######  RED     
lowRed1 = np.array([0, 100, 20], np.uint8)
highRed1 = np.array([8, 255, 255], np.uint8)
lowRed2 = np.array([175, 100, 20], np.uint8)
highRed2 = np.array([180, 255, 255], np.uint8)
#### Orange #############
#lowOrange = np.array([11, 100, 20], np.uint8)
#highOrange = np.array([19, 255, 255], np.uint8)
#### Yellow #############
lowYellow = np.array([20, 100, 20], np.uint8)
highYellow = np.array([32, 255, 255], np.uint8)
### Green #############
lowGreen = np.array([36, 100, 20], np.uint8)
highGreen = np.array([70, 255, 255], np.uint8)
###### Purple ####
lowPurple = np.array([130, 100, 20], np.uint8)
highPurple = np.array([145, 255, 255], np.uint8)
##### Pink ######
lowPink = np.array([146, 100, 20], np.uint8)
highPink = np.array([170, 255, 255], np.uint8)
###### Blue #############
lowBlue = np.array([100,100,20],np.uint8)
highBlue = np.array([125,255,255],np.uint8)
#### all   #############


while True:
    ret,frame = cap.read()
    if ret == True:
      frameHSV = cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)
      ##  RED   ################
      maskRed1 = cv2.inRange(frameHSV,lowRed1,highRed1)
      maskRed2 = cv2.inRange(frameHSV,lowRed2,highRed2)
      maskRed = cv2.add(maskRed1,maskRed2)
      ##  BLUE    ##########
      maskBlue = cv2.inRange(frameHSV,lowBlue,highBlue)
      ### GReen   ########
      maskGreen=cv2.inRange(frameHSV,lowGreen,highGreen)
      ##  Yellow  #########
      maskYellow = cv2.inRange(frameHSV,lowYellow,highYellow)
      #### Orange ################
      #maskOrange = cv2.inRange(frameHSV,lowOrange,highOrange)
      ######## Pink  ########
      maskPink = cv2.inRange(frameHSV,lowPink,highPink)
      ######### Purple #############
      maksPurple = cv2.inRange(frameHSV,lowPurple,highPurple)
      
      ############################################################################
      
      maskBlueRed=cv2.add(maskRed,maskBlue)
      maskGreenYellow=cv2.add(maskGreen,maskYellow)
      maskPinkPurple=cv2.add(maskPink,maksPurple)
      mask4=cv2.add(maskBlueRed,maskGreenYellow)
      
      maskAll=cv2.add(mask4,maskPinkPurple)
      
      ###########################################################

      ##### Comentar los colores que no deseas mostrar ######
      #draw(maskRed,(0,0,255))
      #draw(maskBlue,(255,0,0))
      #draw(maskGreen,(0,255,0))
      #draw(maskYellow,(0,255,255))
      #draw(maskPink,(237,0,240))
      #draw(maksPurple,(129,43,255))
      draw(maskAll,(0,0,0))
      cv2.imshow('Color Detector',frame)
      if cv2.waitKey(1) & 0xFF == ord('q'):
        break
cap.release()
cv2.destroyAllWindows()